<page xmlns="http://projectmallard.org/1.0/"
      type="topic"
      id="workflow">

  <info>
    <link type="guide" xref="index#start"/>
  </info>

  <title>Workflow</title>

  <p>Managing tasks is very personal. Therefore Endeavour only ships with a default Workflow that can be altered to the needs of the user.</p>

  <section>
    <title>Task lifecycle</title>
    <p>Tasks follow a lifecycle that consists of 3 phases:</p>

    <steps>
      <item><p>Capture</p></item>
      <item><p>Processing</p></item>
      <item><p>Execution</p></item>
    </steps>

    <p>Each phase changes the state of the task, starting from the uncaptured state, leading up to the task completion.</p>
  </section>

  <section>
    <title>1. Capturing</title>
    <p>Capturing a task is the act of storing the task in a permanent storage - your hard drive.</p>
    <p>Captured tasks go to the inbox, which is the list of unprocessed tasks.</p>
  </section>

  <section>
    <title>2. Processing</title>
    <p>Processing a task consists of moving it to an appropriate task list, or doing it right away if it's a trivial task, or even throwing it in the wastebasket if you don't plan on doing it.</p>
    <p>Optionally, adding more description, setting an end date, and marking it as important are done in this step.</p>
  </section>

  <section>
    <title>3. Executing</title>
    <p>Executing a task is what leads the task to its conclusion.</p>
  </section>

</page>
