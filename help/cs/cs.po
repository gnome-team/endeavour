# Czech translation for gnome-todo.
# Copyright (C) 2021 gnome-todo's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-todo package.
#
# Marek Černocký <marek@manet.cz>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-todo\n"
"POT-Creation-Date: 2021-09-18 10:15+0000\n"
"PO-Revision-Date: 2021-09-26 21:50+0200\n"
"Last-Translator: Marek Černocký <marek@manet.cz>\n"
"Language-Team: Czech <gnome-cs-list@gnome.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: cs\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2\n"
"X-Generator: Gtranslator 3.38.0\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr "Marek Černocký <marek@manet.cz>"

#. (itstool) path: info/desc
#: C/index.page:7
msgid ""
"GNOME To Do is a simple app, with that you can create, process and manage "
"your tasks, to raise your productivity. Also you can synchronize your tasks "
"through your online accounts, so you can check them on all your devices."
msgstr ""
"Úkoly GNOME jsou jednoduchá aplikace, pomocí které můžete vytvářet, řídit a "
"spravovat své úkoly způsobem vedoucím ke zvýšení produktivity. Navíc můžete "
"synchronizovat své úkoly do on-line účtů, takže budete mít přehled na všech "
"svých zařízeních."

#. (itstool) path: page/title
#: C/index.page:10
msgid "<_:media-1/> <span>GNOME To Do</span>"
msgstr "<_:media-1/> <span>Úkoly GNOME</span>"

#. (itstool) path: page/p
#: C/index.page:15
msgid "A simple to-do app for GNOME."
msgstr "Jednoduchý úkolníček pro GNOME."

#. (itstool) path: section/title
#: C/index.page:18
msgid "Getting Started"
msgstr "Začínáme"

#. (itstool) path: section/title
#: C/index.page:22
msgid "Manage Tasks"
msgstr "Správa úkolů"

#. (itstool) path: section/title
#: C/index.page:26
msgid "Manage Task Lists"
msgstr "Správa seznamů úkolů"

#. (itstool) path: section/title
#: C/index.page:30
msgid "Common Problems"
msgstr "Běžné problémy"

#. (itstool) path: section/title
#: C/index.page:34
msgid "Contributing and getting support"
msgstr "Přispívání a získání podpory"

#. (itstool) path: page/title
#: C/add-notes.page:9
msgid "Add notes to a task"
msgstr "Přidání poznámky do úkolu"

#. (itstool) path: item/p
#: C/add-notes.page:12 C/assign-date.page:12
msgid "Expand the task by clicking on it in the list."
msgstr "Kliknutím příslušný úkol v seznamu rozbalte."

#. (itstool) path: item/p
#: C/add-notes.page:13
msgid ""
"Write something in the textfield labeled with <gui style=\"button\">Notes</"
"gui>."
msgstr ""
"Do textového pole s názvem <gui style=\"button\">Poznámky</gui> napište "
"libovolný text."

#. (itstool) path: page/p
#: C/add-notes.page:16
msgid "You can edit this later whenever you want."
msgstr "Kdykoliv později upravit můžete poznámku upravit."

#. (itstool) path: page/title
#: C/archive.page:10
msgid "Archive a task list"
msgstr "Archivace seznamu úkolů"

#. (itstool) path: item/p
#: C/archive.page:14 C/assign-color.page:13 C/delete-tasklist.page:19
#: C/rename-tasklist.page:13
msgid "Navigate to your task list."
msgstr "Přejděte do svého seznamu úkolů."

#. (itstool) path: item/p
#: C/archive.page:15 C/archive.page:23 C/switch-theme.page:15
msgid "Open the menu ( <_:media-1/> ) on the right side of the headerbar."
msgstr "Otevřete nabídku (<_:media-1/>) na pravé straně záhlavní lišty."

#. (itstool) path: item/p
#: C/archive.page:16
msgid ""
"Click on the <gui style=\"button\">Archive</gui> button to archive your task "
"list."
msgstr ""
"Kliknutím na tlačítko <gui style=\"button\">Archivovat</gui> proveďte "
"archivaci seznamu úkolů."

#. (itstool) path: page/p
#: C/archive.page:19
msgid ""
"You can access your archived task lists through the <gui>Archived</gui> "
"category in the utility pane."
msgstr ""
"Ke svým archivovaným seznamům úkolů se dostanete přes kategorii "
"<gui>Archivováno</gui> v obslužném panelu."

#. (itstool) path: page/p
#: C/archive.page:20
msgid "From there you can unarchive them:"
msgstr "Z archivu můžete seznam také vrátit mezi aktivní:"

#. (itstool) path: item/p
#: C/archive.page:24
msgid ""
"The <gui style=\"button\">Unarchive</gui> button moves your task list again "
"to all others."
msgstr ""
"Tlačítko <gui style=\"button\">Vrátit z archivu</gui> přesune seznam úkolů "
"zpět mezi ostatní aktivní."

#. (itstool) path: page/title
#: C/assign-color.page:10
msgid "Assign a color to a task list"
msgstr "Přiřazení barvy seznamu úkolů"

#. (itstool) path: item/p
#: C/assign-color.page:14 C/delete-tasklist.page:20 C/rename-tasklist.page:14
msgid "Open the menu (<_:media-1/>) on the right side of the headerbar."
msgstr "Otevřete nabídku (<_:media-1/>) na pravé straně záhlavní lišty."

#. (itstool) path: item/p
#: C/assign-color.page:15
msgid "Choose one of the given colors by clicking on them."
msgstr "Kliknutím vyberte jednu z nabízených barev."

#. (itstool) path: page/title
#: C/assign-date.page:9
msgid "Assign a date to a task"
msgstr "Přiřazení data k úkolu"

#. (itstool) path: item/p
#: C/assign-date.page:13
msgid ""
"Click on the button labeled with <gui style=\"button\">Today</gui>, <gui "
"style=\"button\">Tomorrow</gui> or with the arrow icon to assign another "
"date."
msgstr ""
"Klikněte na tlačítko s popiskem <gui style=\"button\">Dnes</gui>, nebo <gui "
"style=\"button\">Zítra</gui>, nebo, pokud chcete jiné datum, na ikonu šipky."

#. (itstool) path: page/p
#: C/assign-date.page:16
msgid ""
"To delete the date, click on the button with the arrow icon and then on <gui "
"style=\"button\">None</gui>."
msgstr ""
"Když chcete datum smazat, klikněte na tlačítko s ikonou šipky a pak na <gui "
"style=\"button\">Žádné</gui>."

#. (itstool) path: page/title
#: C/contributing.page:9
msgid "Contribute to GNOME To do"
msgstr "Přispívání do Úkolů GNOME"

#. (itstool) path: page/p
#: C/contributing.page:11
msgid "There are multiple ways of helping to improve GNOME To Do:"
msgstr "Existuje více způsobů, jak pomoci z vylepšováním Úkolů GNOME:"

#. (itstool) path: item/p
#: C/contributing.page:13
msgid "File issues you have encountered while using GNOME To Do"
msgstr "Hlaste problémy, na které jste při používání Úkolů narazili."

#. (itstool) path: item/p
#: C/contributing.page:14
msgid "Translate the user interface or help pages of GNOME To Do"
msgstr ""
"Překládejte uživatelské rozhraní, případně nápovědu, Úkolů do svého jazyka."

#. (itstool) path: item/p
#: C/contributing.page:15
msgid "Correct issues or implement new features"
msgstr "Opravujte chyby nebo implementujte nové funkce."

#. (itstool) path: page/p
#: C/contributing.page:18
msgid ""
"For filing an issue go to the <link href=\"https://gitlab.gnome.org/gnome/"
"gnome-todo/issues\">issuetracker</link>."
msgstr ""
"Když chcete nahlásit problém, použijte <link href=\"https://gitlab.gnome.org/"
"gnome/gnome-todo/issues\">systém pro sledování chyb</link>."

#. (itstool) path: page/p
#: C/contributing.page:19
msgid ""
"And for contributing code or translations go to the <link href=\"https://"
"wiki.gnome.org/action/show/Newcomers\">newcomers guide</link>."
msgstr ""
"A jestli chcete přispívat do kódu nebo do překladů, podívejte se na <link "
"href=\"https://wiki.gnome.org/action/show/Newcomers\">pokyny pro "
"začátečníky</link>."

#. (itstool) path: page/title
#: C/create-tasklists.page:9
msgid "Create a task list"
msgstr "Vytvoření seznamu úkolů"

#. (itstool) path: item/p
#: C/create-tasklists.page:12
msgid ""
"Click on the <gui style=\"button\">New List</gui> button in the headerbar."
msgstr ""
"Klikněte na tlačítko <gui style=\"button\">Nový seznam</gui> v záhlavní "
"liště."

#. (itstool) path: item/p
#: C/create-tasklists.page:13
msgid "Type in the name of your task list."
msgstr "Napište název svého seznamu úkolů."

#. (itstool) path: item/p
#: C/create-tasklists.page:14
msgid ""
"Then hit <keyseq><key>Enter</key></keyseq> or click on <gui style=\"button"
"\">Create List</gui>."
msgstr ""
"Pak zmáčkněte <keyseq><key>Enter</key></keyseq> nebo klikněte na <gui style="
"\"button\">Vytvořit seznam</gui>."

#. (itstool) path: page/p
#: C/create-tasklists.page:17
msgid "Now you can access your list through the entries in the utility pane."
msgstr ""
"Ke svému seznamu se teď můžete dostat přes příslušnou položku v obslužném "
"panelu."

#. (itstool) path: page/title
#: C/create-tasks.page:9
msgid "Create a Task"
msgstr "Vytvoření úkolu"

#. (itstool) path: item/p
#: C/create-tasks.page:12
msgid ""
"Type the title of the new task in the textfield labeled with <gui style="
"\"button\">New Task</gui> next to + ."
msgstr ""
"Do textového pole <gui style=\"button\">Nový úkol</gui>, vedle symbolu +, "
"napište název nového úkolu."

#. (itstool) path: item/p
#: C/create-tasks.page:13
msgid "Then hit <keyseq><key>Enter</key></keyseq> or click on + to save it."
msgstr ""
"Pro jeho uložení zmáčkněte <keyseq><key>Enter</key></keyseq> nebo klikněte "
"na +."

#. (itstool) path: note/title
#: C/create-tasks.page:17 C/delete-tasklist.page:13
msgid "Undoing changes"
msgstr "Vracení změn"

#. (itstool) path: note/p
#: C/create-tasks.page:18
msgid ""
"Note that you can always undo deleting a task if you change your mind. Click "
"<gui style=\"button\">Undo</gui> on the pop-up notification that appears. "
"But when the popup disappears, you cannot recover it anymore."
msgstr ""
"Pamatujte, že vždy dostanete šanci vrátit odstraněny úkol zpět, pokud si to "
"rozmyslíte. Stačí kliknout na <gui style=\"button\">Zpět</gui> v upozornění, "
"které se objeví. Ale jakmile upozornění zmizí, již není žádná možnost, jak "
"úkol obnovit."

#. (itstool) path: page/p
#: C/create-tasks.page:22
msgid ""
"When you want to delete the task, expand the task by clicking on it in the "
"list and then click on <gui style=\"button\">Delete</gui>."
msgstr ""
"Když chcete úkol odstranit, rozbalte jej kliknutím na něj v seznamu a pak "
"klikněte na <gui style=\"button\">Odstranit</gui>."

#. (itstool) path: page/title
#: C/delete-tasklist.page:10
msgid "Delete a task list"
msgstr "Odstranění seznamu úkolů"

#. (itstool) path: note/p
#: C/delete-tasklist.page:14
msgid ""
"Note that you can always undo deleting a task list if you change your mind. "
"Click <gui style=\"button\">Undo</gui> on the pop-up notification that "
"appears. But when the popup disappears, you cannot recover it anymore."
msgstr ""
"Pamatujte, že vždy dostanete šanci vrátit odstraněny seznam úkolů zpět, "
"pokud si to rozmyslíte. Stačí kliknout na <gui style=\"button\">Zpět</gui> v "
"upozornění, které se objeví. Ale jakmile upozornění zmizí, již není žádná "
"možnost, jak seznam obnovit."

#. (itstool) path: item/p
#: C/delete-tasklist.page:21
msgid ""
"Then the <gui style=\"button\">Delete</gui> button deletes your task list."
msgstr ""
"Tlačítkem <gui style=\"button\">Odstranit</gui> seznam úkolů odstraníte."

#. (itstool) path: page/title
#: C/execute-task.page:9
msgid "Execute a task"
msgstr "Spuštění úkolu"

#. (itstool) path: page/p
#: C/execute-task.page:11
msgid ""
"Click on the empty field on the left side of the task in the list, to give "
"it a check. Then it disappears from <gui>Inbox</gui>, and similar "
"categories. When the task was in a task list, you can still find it there "
"and remove the check to make it again not executed."
msgstr ""
"Kliknutím na prázdný čtvereček před názvem úkolu úkol zaškrtnete a tím je "
"považován za dokončený. Zmizí z <gui>Přijatých</gui> a dalších podobných "
"kategorií. Pokud je ale součástí některého z vašich seznamů úkolů, nadále v "
"něm zůstane a zrušením zaškrtnutí jej můžete znovu vrátit mezi prováděné "
"úkoly."

#. (itstool) path: page/title
#: C/give-star.page:9
msgid "Give a star to a task"
msgstr "Přidělování hvězdiček úkolům"

#. (itstool) path: page/p
#: C/give-star.page:11
msgid ""
"Sometimes you have tasks with a very high priority or want to find them "
"easier in a long list. In that case you can give tasks a star."
msgstr ""
"Někdy můžete mít úkol s vysokou prioritou, nebo jej prostě potřebujete "
"dokázat rychle najít v dlouhém seznamu. V takových případech můžete úkolu "
"přidělit hvězdičku."

#. (itstool) path: page/p
#: C/give-star.page:13
msgid ""
"Click on the small star on the right side of the task in the list to toggle "
"its state. When it is filled you have starred it."
msgstr ""
"Kliknutím přepněte stav malé hvězdičky po pravé straně úkolu v seznamu. Když "
"je hvězdička vyplněná, je přidělená."

#. (itstool) path: page/title
#: C/overview.page:9
msgid "Overview"
msgstr "Přehled"

#. (itstool) path: page/p
#: C/overview.page:13
msgid ""
"The user interface of GNOME To Do is composed of the utility pane on the "
"left and the tasks in lists on the right. You can let you show your tasks in "
"different modes. They can be selected through the utility pane. <gui>Inbox</"
"gui> contains all tasks which have no date assigned to them and are not in a "
"task list, <gui>Today</gui> all tasks with the date of today and <gui>Next 7 "
"Days</gui> all tasks assigned to one of the next seven days. <gui>All</gui> "
"shows all tasks in a chronological order and <gui>Archived</gui> archived "
"task lists. At least there are also your custom task lists"
msgstr ""
"Uživatelské rozhraní Úkolů GNOME se skládá z obslužného panelu vlevo a úkolů "
"v seznamu vpravo. Úkoly si můžete zobrazit v různých režimech. Ty se "
"vybírají v obslužném panelu. V <gui>Přijatých</gui> najdete všechny úkoly, "
"které nemají přidělené datum a nejsou rovněž v žádném seznamu úkolů. V "
"<gui>Dnes</gui> jsou úkoly s dnešním datem dokončení a v <gui>Příštích 7 "
"dní</gui> úkoly s datem dokončení v následujících sedmi dnech. <gui>Vše</"
"gui> pak obsahuje všechny úkoly v chronologickém pořadí a <gui>Archivováno</"
"gui> archivované seznamy úkolů. A nakonec jsou zde vaše vlastní seznamy "
"úkolů."

#. (itstool) path: page/title
#: C/rename-tasks.page:9
msgid "Rename a Task"
msgstr "Přejmenování úkolu"

#. (itstool) path: item/p
#: C/rename-tasks.page:12
msgid "Go to the title of the task in the list, the cursor should change then."
msgstr "Najeďte na název úkolu v seznamu a měla by se změnit podoba ukazatele."

#. (itstool) path: item/p
#: C/rename-tasks.page:13
msgid "Click on it, and then begin renaming it."
msgstr "Klikněte na něj a můžete přejmenovávat."

#. (itstool) path: item/p
#: C/rename-tasks.page:14
msgid "When you are ready, hit <keyseq><key>Enter</key></keyseq>."
msgstr "Až jste hotovi, zmáčkněte <keyseq><key>Enter</key></keyseq>."

#. (itstool) path: page/title
#: C/rename-tasklist.page:10
msgid "Rename a task list"
msgstr "Přejmenování seznamu úkolů"

#. (itstool) path: item/p
#: C/rename-tasklist.page:15
msgid "Click on the <gui style=\"button\">Rename</gui> button."
msgstr "Klikněte na tlačítko <gui style=\"button\">Přejmenovat</gui>."

#. (itstool) path: item/p
#: C/rename-tasklist.page:16
msgid "Type in the new name."
msgstr "Napište nový název."

#. (itstool) path: item/p
#: C/rename-tasklist.page:17
msgid ""
"Hit <keyseq><key>Enter</key></keyseq> or click <gui style=\"button\">Rename</"
"gui>."
msgstr ""
"Zmáčkněte <keyseq><key>Enter</key></keyseq> nebo klikněte na <gui style="
"\"button\">Přejmenovat</gui>."

#. (itstool) path: page/title
#: C/support.page:9
msgid "Getting support"
msgstr "Získání podpory"

#. (itstool) path: page/p
#: C/support.page:11
msgid ""
"In the <link href=\"https://wiki.gnome.org/apps/todo\">wiki</link> of GNOME "
"To Do you can read more."
msgstr ""
"Více se můžete dočíst na <link href=\"https://wiki.gnome.org/apps/todo"
"\">wiki</link> projektu Úkoly GNOME."

#. (itstool) path: page/p
#: C/support.page:12
msgid ""
"When you have questions, ask them on <link href=\"https://discourse.gnome."
"org/tag/todo\">gnome discourse</link>."
msgstr ""
"Když máte nějakou otázku, položte ji v <link href=\"https://discourse.gnome."
"org/tag/todo\">gnome discourse</link>."

#. (itstool) path: page/p
#: C/support.page:13
msgid ""
"You can also get in conversation with the developers of GNOME To Do by "
"joining the <link href=\"https://gnome.element.io/#/room/#gnome-todo:gnome."
"org\">matrix channel</link>."
msgstr ""
"Po připojení ke <link href=\"https://gnome.element.io/#/room/#gnome-todo:"
"gnome.org\">kanálu Matrix</link> můžete také komunikovat s vývojáři Úkolů "
"GNOME."

#. (itstool) path: page/title
#: C/switch-theme.page:10
msgid "Switch to the dark theme"
msgstr "Přepnutí do tmavého motivu"

#. (itstool) path: page/p
#: C/switch-theme.page:12
msgid ""
"When you are in a dark environment, for example at night, a dark theme can "
"be much more comfortable for the eyes."
msgstr ""
"Když se nacházíte v tmavém prostředí, například je noc, může být pro oči "
"příjemnější tmavý motiv."

#. (itstool) path: item/p
#: C/switch-theme.page:16
msgid "Click on the dark or light circle, and the theme switches to it."
msgstr "Kliknutím na tmavé nebo světlé kolečko si motiv přepněte."

#. (itstool) path: page/title
#: C/workflow.page:9
msgid "Workflow"
msgstr "Postup práce"

#. (itstool) path: page/p
#: C/workflow.page:11
msgid ""
"Managing tasks is very personal. Therefore GNOME To Do only ships with a "
"default Workflow that can be altered to the needs of the user."
msgstr ""
"Správu úkolů si řeší každý jinak, podle osobních potřeb. Proto je v Úkolech "
"GNOME přednastavený pouze základní postup práce s úkolem, který se dá podle "
"potřeb uživatele upravit."

#. (itstool) path: section/title
#: C/workflow.page:14
msgid "Task lifecycle"
msgstr "Cyklus úkolu"

#. (itstool) path: section/p
#: C/workflow.page:15
msgid "Tasks follow a lifecycle that consists of 3 phases:"
msgstr "Životní cyklus úkolu prochází 3 fázemi:"

#. (itstool) path: item/p
#: C/workflow.page:18
msgid "Capture"
msgstr "Zaznamenání"

#. (itstool) path: item/p
#: C/workflow.page:19
msgid "Processing"
msgstr "Zpracování"

#. (itstool) path: item/p
#: C/workflow.page:20
msgid "Execution"
msgstr "Provedení"

#. (itstool) path: section/p
#: C/workflow.page:23
msgid ""
"Each phase changes the state of the task, starting from the uncaptured "
"state, leading up to the task completion."
msgstr ""
"Jednotlivé fáze mění stav úkolu, počínaje od nezaznamenaného úkolu až po "
"závěrečné dokončení úkolu."

#. (itstool) path: section/title
#: C/workflow.page:27
msgid "1. Capturing"
msgstr "1. Zaznamenání"

#. (itstool) path: section/p
#: C/workflow.page:28
msgid ""
"Capturing a task is the act of storing the task in a permanent storage - "
"your hard drive."
msgstr ""
"Zaznamenání úkolu je úkon zápisu úkolu do trvalého úložiště – na váš disk."

#. (itstool) path: section/p
#: C/workflow.page:29
msgid "Captured tasks go to the inbox, which is the list of unprocessed tasks."
msgstr ""
"Zaznamenané úkoly se objeví v příchozích, což je seznam nezpracovaných úkolů."

#. (itstool) path: section/title
#: C/workflow.page:33
msgid "2. Processing"
msgstr "2. Zpracování"

#. (itstool) path: section/p
#: C/workflow.page:34
msgid ""
"Processing a task consists of moving it to an appropriate task list, or "
"doing it right away if it's a trivial task, or even throwing it in the "
"wastebasket if you don't plan on doing it."
msgstr ""
"Zpracování úkolu spočívá v jeho přesunutí do příslušného seznamu úkolů, nebo "
"v jeho okamžitém provedení, pokud se jedná o jednoduchý úkol. Případně jej "
"můžete zahodit, pokud se rozhodnete jej nerealizovat."

#. (itstool) path: section/p
#: C/workflow.page:35
msgid ""
"Optionally, adding more description, setting an end date, and marking it as "
"important are done in this step."
msgstr ""
"V tomto kroku můžete úkolu volitelně přidat popis, nastavit datum dokončení "
"a označit jej jako důležitý."

#. (itstool) path: section/title
#: C/workflow.page:39
msgid "3. Executing"
msgstr "3. Provedení"

#. (itstool) path: section/p
#: C/workflow.page:40
msgid "Executing a task is what leads the task to its conclusion."
msgstr "Provedení úkolu je to, co vede k jeho dokončení."
