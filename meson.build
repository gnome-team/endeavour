project(
     'endeavour',
              'c',
          version: '43',
          license: 'GPL3+',
  default_options: [ 'buildtype=debugoptimized', 'warning_level=1' ],
    meson_version: '>= 0.57.0'
)

endeavour_version = meson.project_version()

endeavour_gir_namespace = 'Gtd'
endeavour_gir_version = '1.0'

endeavour_prefix = get_option('prefix')
endeavour_bindir = join_paths(endeavour_prefix, get_option('bindir'))
endeavour_datadir = join_paths(endeavour_prefix, get_option('datadir'))
endeavour_includedir = join_paths(endeavour_prefix, get_option('includedir'))
endeavour_libdir = join_paths(endeavour_prefix, get_option('libdir'))
endeavour_libexecdir = join_paths(endeavour_prefix, get_option('libexecdir'))
endeavour_localedir = join_paths(endeavour_prefix, get_option('localedir'))

endeavour_pkgdatadir = join_paths(endeavour_datadir, meson.project_name())
endeavour_pkgincludedir = join_paths(endeavour_includedir, meson.project_name())
endeavour_pkglibdir = join_paths(endeavour_libdir, meson.project_name())

endeavour_pluginsdir = join_paths(endeavour_pkglibdir, 'plugins')
endeavour_schemadir = join_paths(endeavour_datadir, 'glib-2.0', 'schemas')

soversion = 0
current = 0
revision = 0
libversion = '@0@.@1@.@2@'.format(soversion, current, revision)

endeavour_debug = get_option('buildtype').contains('debug')
endeavour_tracing = get_option('tracing')

cc = meson.get_compiler('c')

if get_option('profile') == 'development'
  profile = 'Devel'
  application_id = 'org.gnome.Todo.Devel'
else
  profile = ''
  name_suffix = ''
  application_id = 'org.gnome.Todo'
endif

config_h = configuration_data()

config_h.set_quoted('GETTEXT_PACKAGE', meson.project_name())

# development build
config_h.set('DEVELOPMENT_BUILD', profile == 'Devel')

# debug options
config_h.set('ENDEAVOUR_ENABLE_DEBUG', endeavour_debug)
config_h.set('ENABLE_TRACING', endeavour_tracing)
config_h.set('NDEBUG', not endeavour_debug)

# package
set_defines = [
  ['APPLICATION_ID', application_id],
  ['AUTOSTART_FILE', application_id + '.Autostart.desktop'],
  ['PACKAGE', meson.project_name()],
  ['PACKAGE_BUGREPORT', 'https://gitlab.gnome.org/GNOME/' + meson.project_name() + '/issues/new'],
  ['PACKAGE_NAME', meson.project_name()],
  ['PACKAGE_STRING', '@0@ @1@'.format(meson.project_name(), endeavour_version)],
  ['PACKAGE_TARNAME', meson.project_name()],
  ['PACKAGE_URL', 'https://wiki.gnome.org/Apps/Todo'],
  ['PACKAGE_VERSION', meson.project_version()],
  ['VERSION', meson.project_version()],
  # i18n
  ['GETTEXT_PACKAGE', meson.project_name()]
]

foreach define: set_defines
  config_h.set_quoted(define[0], define[1])
endforeach

# headers
check_headers = [
  ['HAVE_DLFCN_H', 'dlfcn.h'],
  ['HAVE_INTTYPES_H', 'inttypes.h'],
  ['HAVE_LOCALE_H', 'locale.h'],
  ['HAVE_MEMORY_H', 'memory.h'],
  ['HAVE_STDINT_H', 'stdint.h'],
  ['HAVE_STDLIB_H', 'stdlib.h'],
  ['HAVE_STRINGS_H', 'strings.h'],
  ['HAVE_STRING_H', 'string.h'],
  ['HAVE_SYS_STAT_H', 'sys/stat.h'],
  ['HAVE_SYS_TYPES_H', 'sys/types.h'],
  ['HAVE_UNISTD_H', 'unistd.h']
]

foreach header: check_headers
  config_h.set(header[0], cc.has_header(header[1]))
endforeach

# functions
check_functions = [
  # i18n
  ['HAVE_DCGETTEXT', 'dcgettext'],
  ['HAVE_GETTEXT', 'gettext'],
  ['HAVE_ICONV', 'iconv']
]

if host_machine.system().contains('darwin')
  check_functions += [
    ['HAVE_CFLOCALECOPYCURRENT', 'CFLocaleCopyCurrent'],
    ['HAVE_CFPREFERENCESCOPYAPPVALUE', 'CFPreferencesCopyAppValue']
  ]
endif

foreach func: check_functions
  config_h.set(func[0], cc.has_function(func[1]))
endforeach

# compiler flags
common_flags = ['-DHAVE_CONFIG_H']
compiler_flags = []

if endeavour_debug
  test_cflags = [
    '-Wno-error=unused-parameter', '-Wno-unused-parameter',
    '-Wno-error=missing-field-initializers', '-Wno-missing-field-initializers',
  ]

  foreach cflag: test_cflags
    if cc.has_argument(cflag)
      compiler_flags += [cflag]
    endif
  endforeach
endif

add_project_arguments(common_flags + compiler_flags, language: 'c')


#######################
# Shared dependencies #
#######################

glib_dep = dependency('glib-2.0', version: '>= 2.58.0')
gtk_dep = dependency('gtk4', version: '>= 3.92.0')

libadwaita_dep = dependency(
  'libadwaita-1',
  version: '>= 1.2.alpha',
  fallback: ['libadwaita', 'libadwaita_dep'],
  default_options: ['examples=false', 'introspection=disabled', 'tests=false', 'vapi=false'],
)

endeavour_deps = [
  glib_dep,
  gtk_dep,
  libadwaita_dep,
  dependency('gio-2.0', version: '>= 2.43.4'),
  dependency('gmodule-export-2.0'),
  dependency('goa-1.0', version: '>= 3.2.0'),
  dependency('libpeas-1.0', version: '>= 1.17'),
  cc.find_library('m', required: true)
]

gnome = import('gnome')
i18n = import('i18n')
pkg = import('pkgconfig')

data_dir = join_paths(meson.project_source_root(), 'data')
icons_dir = join_paths(data_dir, 'icons')
po_dir = join_paths(meson.project_source_root(), 'po')
plugins_dir = join_paths(meson.project_source_root(), 'plugins')

subdir('data')
subdir('src')
subdir('po')
subdir('tests')
subdir('help')

gnome.post_install(
  glib_compile_schemas: true,
  gtk_update_icon_cache: true,
)

summary({
  'Source': meson.project_source_root(),
  'Prefix': endeavour_prefix,
})

summary({
  'Debug': endeavour_debug,
  'Tracing': endeavour_tracing,
}, section: 'Development')
